import { Images, Price } from '@spartacus/core';

export const RECENTLY_VIEWD_PRODUCTS_KEY = 'recently-viewed-products';
export const KEY_NAME = 'name';

export interface ProductInfo {
  code: string;
  name: string;
  price: Price;
  images: Images;
}

export interface RecentlyViewedProducts {
  maxSliderItems: number;
}

export interface FeaturesConfig {
  recentlyViewedProducts: RecentlyViewedProducts;
}


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";

import { AppRoutingModule } from './app-routing.module';
import { SpartacusModule } from './spartacus/spartacus.module';
import { AppComponent } from './app.component';

import { FEATURES_CONFIG_TOKEN, featuresConfig } from './configs/features-config';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    SpartacusModule
  ],
  providers: [
    {
      provide: FEATURES_CONFIG_TOKEN,
      useValue: featuresConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

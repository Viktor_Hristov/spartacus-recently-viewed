import { Inject, Injectable } from '@angular/core';

import { CurrentProductService } from '@spartacus/storefront';
import { Product, StatePersistenceService } from '@spartacus/core';

import { FEATURES_CONFIG_TOKEN, featuresConfig } from './configs/features-config';
import { ProductInfo, KEY_NAME, RECENTLY_VIEWD_PRODUCTS_KEY, FeaturesConfig } from './models/features-model';

import { BehaviorSubject, Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

import { has, applySpec, prop, clone, slice } from 'ramda';


const hasName = has(KEY_NAME);
const productSubset = applySpec<ProductInfo>({
  code: prop('code'),
  name: prop('name'),
  price: prop('price'),
  images: prop('images'),
});

@Injectable({
  providedIn: 'root'
})
export class RecentlyViewedService {

  readonly recentlyViewed$: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  private recentlyViewedObservable: Observable<Product[]> = this.recentlyViewed$.asObservable();

  public products$: Observable<Product> = this.currentProduct.getProduct().pipe(
    filter(Boolean),
    filter(hasName),
    tap((product: Product) => this.refreshRecentlyViewedProducts(product)),
  );

  private visibleItemsIndex = 0;

  constructor(
    private currentProduct: CurrentProductService,
    private statePersistenceService: StatePersistenceService,
    @Inject(FEATURES_CONFIG_TOKEN) private config: FeaturesConfig,
  ) {
    this.visibleItemsIndex = (this.config.recentlyViewedProducts.maxSliderItems - 1);
  }

  init(): void {
    this.products$.subscribe();

    this.statePersistenceService.syncWithStorage({
      key: RECENTLY_VIEWD_PRODUCTS_KEY,
      state$: this.recentlyViewed$,
      onRead: (recentlyViewed) => {
        this.recentlyViewed$.next(recentlyViewed ? recentlyViewed : []);
      }
    });
  }

  recentlyViewedProducts(): Observable<Product[]> {
    return this.recentlyViewedObservable;
  }

  refreshRecentlyViewedProducts(product: Product): void {
    const domainProduct = clone(productSubset(product));
    let viewedProducts = this.recentlyViewed$.getValue();
    viewedProducts = slice(0, this.visibleItemsIndex, viewedProducts);
    this.recentlyViewed$.next([domainProduct, ...viewedProducts]);
  }
}

import { InjectionToken } from '@angular/core';

import { FeaturesConfig } from './../models/features-model';

const FEATURES_CONFIG = 'Features Configuration';
export const FEATURES_CONFIG_TOKEN = new InjectionToken<FeaturesConfig>(FEATURES_CONFIG);

export const featuresConfig: Readonly<FeaturesConfig> = Object.freeze({
  recentlyViewedProducts: {
    maxSliderItems: 6,
  }
});

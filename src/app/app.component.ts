import { Component } from '@angular/core';

import { RecentlyViewedService } from './recently-viewed.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private recentlyViewed: RecentlyViewedService
  ) {
    this.recentlyViewed.init();
  }
}

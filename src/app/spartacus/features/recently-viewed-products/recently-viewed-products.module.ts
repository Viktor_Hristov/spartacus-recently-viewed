import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UrlModule } from '@spartacus/core';
import {
  CarouselModule,
  IconModule,
  MediaModule,
  ProductListModule,
  OutletRefModule,
} from '@spartacus/storefront';

import {
  RecentlyViewedProductsComponent
} from './recently-viewed-products.component';



@NgModule({
  declarations: [
    RecentlyViewedProductsComponent
  ],
  imports: [
    CommonModule,
    OutletRefModule,
    ProductListModule,
    RouterModule,
    CarouselModule,
    MediaModule,
    UrlModule,
    IconModule,
  ],
  exports: [
    RecentlyViewedProductsComponent
  ],
})
export class RecentlyViewedProductsModule { }

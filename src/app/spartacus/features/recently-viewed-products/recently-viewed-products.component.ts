import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';

import { Product } from '@spartacus/core';
import { ICON_TYPE } from '@spartacus/storefront';

import { RecentlyViewedService } from 'src/app/recently-viewed.service';

import { Observable, of, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-recently-viewed-products',
  templateUrl: './recently-viewed-products.component.html',
  styleUrls: ['./recently-viewed-products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecentlyViewedProductsComponent implements OnDestroy {

  // We don't need this destroy stream, since the only stream is "hooked" with async Pipe
  // This is just for demo purposes instead subscriber: Subject[] way of unsubscribing
  private destroy$ = new Subject<any>();
  private destroyEvent$: Observable<any> = this.destroy$.asObservable();

  public closeIcon: ICON_TYPE = ICON_TYPE.CLOSE;
  public open = true;

  recentlyViewdProducts$: Observable<Observable<Product>[]> = this.recentlyViewed.recentlyViewedProducts().pipe(
    map((products: Product[]) => products.map(p => of(p))),
    takeUntil(this.destroyEvent$),
  );

  constructor(
    private recentlyViewed: RecentlyViewedService,
  ) {}

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}

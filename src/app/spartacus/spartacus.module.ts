import { NgModule } from '@angular/core';

import { BaseStorefrontModule } from "@spartacus/storefront";

import { SpartacusFeaturesModule } from './spartacus-features.module';
import { SpartacusConfigurationModule } from './spartacus-configuration.module';

import { RecentlyViewedProductsComponent } from './features/recently-viewed-products/recently-viewed-products.component';

@NgModule({
  declarations: [],
  imports: [
    SpartacusFeaturesModule,
    SpartacusConfigurationModule,
    BaseStorefrontModule
  ],
  exports: [
    BaseStorefrontModule,
    RecentlyViewedProductsComponent,
  ]
})
export class SpartacusModule { }
